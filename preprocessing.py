from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
import numpy as np

NUM_WORDS=30000

def filter_markdown_quoting(text):

    body_list = text.split("\n")
    body = ""
    for line in body_list:
        if len(line) == 0:
            body = body + "\n"
        elif line[0] != ">":
            body = body + line + "\n"

    return body

def preprocessing_infer(text, tokenizer, labels=None, only_inputs=False):
    tokenizer_args = {"padding": True, "truncation": True, "return_tensors": "np"}
    if only_inputs:
        tokenizer_args["return_token_type_ids"] = False
        tokenizer_args["return_attention_mask"] = False
        tokenizer_args["return_overflowing_tokens"] = False
        tokenizer_args["return_special_tokens_mask"] = False
        tokenizer_args["return_offsets_mapping"] = False
        tokenizer_args["return_length"] = False

    X = tokenizer(text, **tokenizer_args)
    if only_inputs:
        X = X["input_ids"]

    y = None
    if not labels is None:
        y = to_categorical(np.asarray(labels))

    return X, y

def preprocess_labels(label_train, label_val):
    y_train = to_categorical(np.asarray(label_train))
    y_val = to_categorical(np.asarray(label_val))

    return y_train, y_val

def preprocess_text(text, tokenizer, only_inputs=False):
    tokenizer_args = {"padding": 'max_length', "truncation": True, "return_tensors": "np"}
    if only_inputs:
        tokenizer_args["return_token_type_ids"] = False
        tokenizer_args["return_attention_mask"] = False
        tokenizer_args["return_overflowing_tokens"] = False
        tokenizer_args["return_special_tokens_mask"] = False
        tokenizer_args["return_offsets_mapping"] = False
        tokenizer_args["return_length"] = False

    X = tokenizer(text, **tokenizer_args)

    if only_inputs:
        X = X["input_ids"]

    return X

def preprocessing(text_train, text_val, label_train, label_val,
                    text_unlabeled, tokenizer,
                  only_inputs=False):
    assert isinstance(text_train, list)
    assert isinstance(text_val, list)

    tokenizer_args = {"padding": 'max_length', "truncation": True, "return_tensors": "np"}
    if only_inputs:
        tokenizer_args["return_token_type_ids"] = False
        tokenizer_args["return_attention_mask"] = False
        tokenizer_args["return_overflowing_tokens"] = False
        tokenizer_args["return_special_tokens_mask"] = False
        tokenizer_args["return_offsets_mapping"] = False
        tokenizer_args["return_length"] = False

    X_train = tokenizer(text_train, **tokenizer_args)
    X_val = tokenizer(text_val, **tokenizer_args)

    X_train_unl = None
    if not text_unlabeled is None:
        X_train_unl = tokenizer(text_unlabeled, **tokenizer_args)
        if only_inputs:
            X_train_unl = X_train_unl["input_ids"]

    if only_inputs:
        X_train = X_train["input_ids"]
        X_val = X_val["input_ids"]


    print('Found %s unique tokens.' % tokenizer.vocab_size)

    y_train = to_categorical(np.asarray(label_train))
    y_val = to_categorical(np.asarray(label_val))

    return X_train, X_val, y_train, y_val, X_train_unl
