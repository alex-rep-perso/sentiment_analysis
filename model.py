import datetime

import tensorflow as tf
import keras
from keras import layers as kl
from transformers import AlbertTokenizer, TFAlbertModel, AlbertConfig, TFDistilBertModel, DistilBertConfig, \
    DistilBertTokenizerFast
import os

tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)  # Error with logging with tensorflow 1.14 and python 3.8

import gensim
from gensim.models import Word2Vec
from gensim.models.keyedvectors import KeyedVectors
import numpy as np

NUM_WORDS = 25000
PERTURB_NORM_LENGTH = 5.0


def normalize_embedding(tokenizer, embedding_matrix):
    freq_vector = np.zeros((embedding_matrix.shape[0], 1))
    for i in range(1, embedding_matrix.shape[0]):
        freq_vector[i, :] = tokenizer.word_counts[tokenizer.index_word[i]]

    tot_word_occurence = np.sum(np.asarray(list(tokenizer.word_counts.values())).astype(np.float32))

    freq_vector = freq_vector / tot_word_occurence

    mean = np.sum(freq_vector * embedding_matrix, axis=0)

    var = np.sum(freq_vector * (embedding_matrix - mean) ** 2, axis=0)

    return (embedding_matrix - mean) / np.sqrt(var)


def embedding_matrix_word2vec(tokenizer, normalize=False):
    word_vectors = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)

    print("word vectors read")
    embedding_dim = 300

    embedding_matrix = np.zeros((tokenizer.vocab_size, embedding_dim))

    for word, i in tokenizer.get_vocab().items():
        if i >= NUM_WORDS:
            continue
        try:
            embedding_vector = word_vectors[word]
            embedding_matrix[i] = embedding_vector
        except KeyError:
            embedding_matrix[i] = np.random.normal(0, np.sqrt(0.25), embedding_dim)

    del (word_vectors)

    # if normalize:
    #    embedding_matrix = normalize_embedding(tokenizer, embedding_matrix)

    return embedding_matrix


class KFold_LSTM_Model():

    def __init__(self, num_split, *model_args, **model_kwargs):

        assert isinstance(num_split, int) and num_split > 0
        self.num_split = num_split

        self.models = [LSTM_Model(*model_args, **model_kwargs) for _ in range(num_split)]

    def __getitem__(self, idx):

        if idx < 0 or idx >= self.num_split:
            raise ValueError("Index {} is out of bound".format(idx))

        return self.models[idx]

    def predict(self, *args, **kwargs):

        results = [self.models[i].predict(*args, **kwargs) for i in range(self.num_split)]
        if isinstance(results[0], tuple):
            results_comp = [None] * len(results[0])

            for i in range(len(results[0])):
                results_comp[i] = np.mean(np.stack([r[i] for r in results], axis=0), axis=0)

            return tuple(results_comp)

        return np.mean(np.stack(results, axis=0), axis=0)

    def evaluate(self, *args, **kwargs):

        results = [self.models[i].evaluate(*args, **kwargs) for i in range(self.num_split)]
        results_comp = [None] * len(results[0])

        for i in range(len(results[0])):
            results_comp[i] = np.mean(np.stack([r[i] for r in results], axis=0), axis=0)

        return tuple(results_comp)

    def load_weights(self, filename, **kwargs):

        base_name = filename[:-5]

        for idx in range(self.num_split):
            self.models[idx].load_weights(base_name + "_fold_{}.hdf5".format(idx), **kwargs)


class VAT_Model(tf.keras.Model):

    def __init__(self, num_cat, args_layers, use_at=False, use_vat=False, lr=1e-3, at_coeff=0.7,
                 log_dir="logs", name="", *args,
                 **kwargs):

        super().__init__(*args, **kwargs)

        self._tokenizer = None
        self.len_seq = min(2048, self.tokenizer.model_max_length)

        self.num_power_iteration = 1
        self.perturb_norm_length = PERTURB_NORM_LENGTH
        self.small_constant_for_finite_diff = 1e-1

        self.init_layers(args_layers, num_cat)
        self.activation_out = kl.Activation("softmax", name="out_cat")

        self.optimizer = tf.keras.optimizers.Adam(lr=lr)

        self.use_at = use_at
        self.use_vat = use_vat
        self.at_coeff = at_coeff

        self.train_loss = tf.keras.metrics.Mean('train_loss', dtype=tf.float32)
        self.train_cat_loss = tf.keras.metrics.Mean('train_cat_loss', dtype=tf.float32)
        self.train_accuracy = tf.keras.metrics.CategoricalAccuracy("train_accuracy")

        self.val_loss = tf.keras.metrics.Mean('val_loss', dtype=tf.float32)
        self.val_accuracy = tf.keras.metrics.CategoricalAccuracy("val_accuracy")

        if name:
            name = "_" + name

        date_str = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

        self.train_summary_writer = tf.summary.create_file_writer(os.path.join(log_dir, date_str, "train" + name))
        self.val_summary_writer = tf.summary.create_file_writer(os.path.join(log_dir, date_str, "val" + name))

        self.warm_up()

    @property
    def tokenizer(self):
        if self._tokenizer is None:
            self._tokenizer = AlbertTokenizer.from_pretrained('albert-base-v2')

        return self._tokenizer

    def use_simple_inputs(self):

        return True

    def warm_up(self):
        inp = tf.zeros((1, self.len_seq))
        self.__call__(inp)

    def call(self, inputs):

        emb = self.forward_embed(inputs)
        logits = self.forward_model(emb)

        preds = self.activation_out(logits)

        return preds

    @tf.function
    def train_step(self, inputs, labels, weights=None, inp_unlabeled=None):
        with tf.GradientTape() as tape:

            emb = self.forward_embed(inputs)
            logits = self.forward_model(emb)

            emb_unlabeled, logits_unlabeled = tf.constant(0, dtype=tf.float32), tf.constant(0, dtype=tf.float32)
            if self.use_vat and not inp_unlabeled is None:
                emb_unlabeled = self.forward_embed(inp_unlabeled)
                logits_unlabeled = self.forward_model(emb_unlabeled)

            loss = tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=logits)

            if self.use_at:
                logits_perturb = self.adversial_logits(emb, loss)
                loss_at = self.at_coeff * tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=logits_perturb)
            if self.use_vat:
                loss_vat = self.virtual_adversarial_loss(logits_unlabeled, emb_unlabeled)

            if not weights is None:
                total_loss = tf.multiply(weights, loss)
            else:
                total_loss = loss

            if self.use_at:
                total_loss += loss_at
            if self.use_vat:
                total_loss += loss_vat

        gradients = tape.gradient(total_loss, self.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.trainable_variables))

        preds = self.activation_out(logits)

        self.train_loss(total_loss)
        self.train_cat_loss(loss)
        self.train_accuracy(labels, preds)

        return preds

    @tf.function
    def test_step(self, inputs, labels):
        emb = self.forward_embed(inputs)
        logits = self.forward_model(emb)

        loss = tf.nn.softmax_cross_entropy_with_logits(labels=labels, logits=logits)
        preds = self.activation_out(logits)

        self.val_loss(loss)
        self.val_accuracy(labels, preds)

        return preds

    def init_layers(self, args, num_cat):
        raise NotImplementedError

    def forward(self, inp):
        emb = self.forward_embed(inp)
        return self.forward_model(emb)

    def forward_model(self, emb):
        raise NotImplementedError

    def forward_embed(self, inp):
        raise NotImplementedError

    def train_generator(self, generator, steps_per_epoch,
                        epochs=20,
                        validation_data=None, validation_steps=None,
                        initial_epoch=0, save_weights="", callback_lr=None):

        min_val_loss = 100000000000000
        val_loss_list = []
        for epoch in range(initial_epoch, epochs):
            step = 0
            for batch in generator:
                self.train_step(batch["inputs"], batch["labels"], weights=batch["weights"],
                                inp_unlabeled=batch["inputs_unlabeled"])
                step += 1
                print("Step {} out of {} ".format(step, steps_per_epoch), end='\r')
                with self.train_summary_writer.as_default():
                    tf.summary.scalar('loss_step', self.train_loss.result(), step=step)
                    tf.summary.scalar('cat_loss_step', self.train_cat_loss.result(), step=step)
                    tf.summary.scalar('accuracy_step', self.train_accuracy.result(), step=step)
                if step >= steps_per_epoch:
                    break
            print("")
            with self.train_summary_writer.as_default():
                tf.summary.scalar('loss', self.train_loss.result(), step=epoch)
                tf.summary.scalar('cat_loss', self.train_cat_loss.result(), step=epoch)
                tf.summary.scalar('accuracy', self.train_accuracy.result(), step=epoch)

            if not (validation_data is None or validation_steps is None):
                assert isinstance(validation_steps, int) and validation_steps > 0
                val_step = 0
                for batch in validation_data:
                    self.test_step(batch["inputs"], batch["labels"])
                    val_step += 1
                    print("Validation step {} out of {} ".format(val_step, validation_steps), end='\r')
                    if val_step >= validation_steps:
                        break

                print("")
                with self.val_summary_writer.as_default():
                    tf.summary.scalar('cat_loss', self.val_loss.result(), step=epoch)
                    tf.summary.scalar('accuracy', self.val_accuracy.result(), step=epoch)

                template = 'Epoch {}, Loss: {} Cat Loss {}, Accuracy: {}, Val Loss: {}, Val Accuracy: {}'

                print(template.format(epoch + 1,
                                      self.train_loss.result(),
                                      self.train_cat_loss.result(),
                                      self.train_accuracy.result() * 100,
                                      self.val_loss.result(),
                                      self.val_accuracy.result() * 100))

                val_loss = self.val_loss.result()
                if val_loss < min_val_loss:
                    min_val_loss = val_loss

                    if save_weights:
                        self.save_weights(save_weights.format(epoch=epoch))

                val_loss_list.append(val_loss)
                if not callback_lr is None:
                    callback_lr(val_loss_list, self.optimizer.learning_rate)

                self.val_loss.reset_states()
                self.val_accuracy.reset_states()

            # Reset metrics every epoch
            self.train_loss.reset_states()
            self.train_cat_loss.reset_states()
            self.train_accuracy.reset_states()

    def adversial_logits(self, embedded, loss):
        loss = tf.reduce_mean(loss)
        grad = tf.gradients(loss, embedded,
                            aggregation_method=tf.AggregationMethod.EXPERIMENTAL_ACCUMULATE_N)
        grad = tf.stop_gradient(grad)[0]
        perturb = self._scale_l2(grad, self.perturb_norm_length)
        print(perturb.shape.as_list())
        embedded_perturb = embedded + perturb

        logits_perturb = self.forward_model(embedded_perturb)
        return logits_perturb

    """Virtual adversarial loss.
    Computes virtual adversarial perturbation by finite difference method and
    power iteration, adds it to the embedding, and computes the KL divergence
    between the new logits and the original logits.
    Args:
        logits: 3-D float Tensor, [batch_size, num_timesteps, m], m=num_classes.
        embedded: 3-D float Tensor, [batch_size, num_timesteps, embedding_dim].

    Returns:
        kl: float scalar.
    """

    def virtual_adversarial_loss(self, logits, embedded):

        # Stop gradient of logits. See https://arxiv.org/abs/1507.00677 for details.
        logits = tf.stop_gradient(logits)

        # Initialize perturbation with random noise.
        # shape(embedded) = (batch_size, num_timesteps, embedding_dim)
        d = tf.random.normal(shape=tf.shape(embedded))

        # Perform finite difference method and power iteration.
        # See Eq.(8) in the paper http://arxiv.org/pdf/1507.00677.pdf,
        # Adding small noise to input and taking gradient with respect to the noise
        # corresponds to 1 power iteration.
        for _ in range(self.num_power_iteration):
            d = self._scale_l2(d, self.small_constant_for_finite_diff)

        embedded_d = embedded + d
        d_logits = self.forward_model(embedded_d)
        kl_div = tf.reduce_mean(self._kl_divergence_with_logits(logits, d_logits))
        d = tf.gradients(
            kl_div,
            d,
            aggregation_method=tf.AggregationMethod.EXPERIMENTAL_ACCUMULATE_N)[0]
        d = tf.stop_gradient(d)

        perturb = self._scale_l2(d, self.perturb_norm_length)
        embedded_perturb = embedded + perturb

        vadv_logits = self.forward_model(embedded_perturb)

        return self._kl_divergence_with_logits(logits, vadv_logits)

    def _scale_l2(self, x, norm_length):
        # shape(x) = (batch, num_timesteps, d)
        # Divide x by max(abs(x)) for a numerically stable L2 norm.
        # 2norm(x) = a * 2norm(x/a)
        # Scale over the full sequence, dims (1, 2)
        alpha = tf.reduce_max(tf.abs(x), (1, 2), keepdims=True) + 1e-12
        l2_norm = alpha * tf.sqrt(
            tf.reduce_sum(tf.pow(x / alpha, 2), (1, 2), keepdims=True) + 1e-6)
        x_unit = x / l2_norm
        return norm_length * x_unit

    """Returns weighted KL divergence between distributions q and p.
    Args:
        q_logits: logits for 1st argument of KL divergence shape
              [batch_size, num_timesteps, num_classes] if num_classes > 2, and
              [batch_size, num_timesteps] if num_classes == 2.
        p_logits: logits for 2nd argument of KL divergence with same shape q_logits.
        weights: 1-D float tensor with shape [batch_size, num_timesteps].
             Elements should be 1.0 only on end of sequences
    Returns:
        KL: float scalar.
    """

    def _kl_divergence_with_logits(self, q_logits, p_logits):

        q = tf.nn.softmax(q_logits)
        p = tf.nn.softmax(p_logits)

        return keras.losses.kullback_leibler_divergence(q, p)


class LSTM_Model(VAT_Model):

    def __init__(self, num_cat, args_layers,
                 use_at=False,
                 use_vat=False,
                 lr=1e-3,
                 at_coeff=0.7,
                 name="lstm"):

        super(LSTM_Model, self).__init__(num_cat, args_layers,
                                         use_at=use_at,
                                         use_vat=use_vat,
                                         lr=lr,
                                         at_coeff=at_coeff,
                                         name=name)

    def init_layers(self, args, num_cat):
        if not args["init_embedding_matrix"]:
            self.emb_layer = kl.Embedding(self.tokenizer.vocab_size, args["embedding_dim"],
                                          input_length=self.len_seq, mask_zero=True,
                                          trainable=args["trainable_embedding"])
        else:
            embedding = embedding_matrix_word2vec(self.tokenizer, normalize=True)
            self.emb_layer = kl.Embedding(self.tokenizer.vocab_size, args["embedding_dim"],
                                          weights=[embedding],
                                          trainable=args["trainable_embedding"], mask_zero=True,
                                          input_length=self.len_seq)

        print("embedding done")
        self.lstm_1 = kl.Bidirectional(kl.LSTM(args["hidden_dim"], return_sequences=True))
        self.lstm_2 = kl.Bidirectional(kl.LSTM(args["hidden_dim"]))

        self.dense_latent = kl.Dense(args["hidden_dim"] // 2, activation="relu")
        self.dense_logits = kl.Dense(num_cat, name="dense_logits_{}".format(num_cat))

        self.dropout = kl.Dropout(0.5)

    def forward_model(self, emb):
        x = self.lstm_2(emb)
        x = self.dense_latent(x)
        x = self.dense_logits(x)

        return x

    def forward_embed(self, inp):
        return self.dropout(self.emb_layer(inp))


class DistilBERT_Model(VAT_Model):

    def __init__(self, num_cat, args_layers,
                 use_at=False,
                 use_vat=False,
                 lr=1e-3,
                 at_coeff=0.7,
                 name="distilbert"):
        super(DistilBERT_Model, self).__init__(num_cat, args_layers,
                                               use_at=use_at,
                                               use_vat=use_vat,
                                               lr=lr,
                                               at_coeff=at_coeff,
                                               name=name)

    def use_simple_inputs(self):
        return False

    @property
    def tokenizer(self):
        if self._tokenizer is None:
            self._tokenizer = DistilBertTokenizerFast.from_pretrained('distilbert-base-uncased')

        return self._tokenizer

    def warm_up(self):
        inp = tf.zeros((1, self.len_seq), dtype=tf.int32)
        self.__call__({"input_ids": inp, "attention_mask": inp})

    def init_layers(self, args, num_cat):
        self.config = DistilBertConfig.from_pretrained("distilbert-base-uncased")
        self.config.return_dict = True
        self.distilbert_model = TFDistilBertModel.from_pretrained("distilbert-base-uncased")
        self.embeddings = self.distilbert_model.get_input_embeddings()

        self.average = kl.GlobalAveragePooling1D()
        self.dense_latent = kl.Dense(self.config.dim, activation="relu")
        self.dense_logits = kl.Dense(num_cat, name="dense_logits_{}".format(num_cat))

        self.cur_attention_mask = None

    def forward_model(self, emb):
        head_mask = [None] * self.config.num_hidden_layers

        enc_out = self.distilbert_model.distilbert.transformer(emb, self.cur_attention_mask, head_mask,
                                                               None, None, False)[0]

        x = enc_out[:, 0]
        x = self.dense_latent(x)
        x = self.dense_logits(x)

        return x

    def forward_embed(self, inp):
        embedding_out = self.embeddings(input_ids=inp["input_ids"])
        self.cur_attention_mask = inp["attention_mask"]

        return embedding_out


class ALBERT_Model(VAT_Model):

    def __init__(self, num_cat, args_layers,
                 use_at=False,
                 use_vat=False,
                 lr=1e-3,
                 at_coeff=0.7,
                 name="albert"):
        super(ALBERT_Model, self).__init__(num_cat, args_layers,
                                           use_at=use_at,
                                           use_vat=use_vat,
                                           lr=lr,
                                           at_coeff=at_coeff,
                                           name=name)

    def use_simple_inputs(self):
        return False

    @property
    def tokenizer(self):
        if self._tokenizer is None:
            self._tokenizer = AlbertTokenizer.from_pretrained('albert-base-v2')

        return self._tokenizer

    def warm_up(self):
        inp = tf.zeros((1, self.len_seq), dtype=tf.int32)
        self.__call__({"input_ids": inp, "attention_mask": inp, "token_type_ids": inp})

    def init_layers(self, args, num_cat):
        self.config = AlbertConfig.from_pretrained("albert-base-v2")
        self.config.return_dict = True
        self.albert_model = TFAlbertModel.from_pretrained("albert-base-v2")
        self.embeddings = self.albert_model.get_input_embeddings()

        self.average = kl.GlobalAveragePooling1D()
        self.dense_latent = kl.Dense(self.config.hidden_size, activation="relu")
        self.dense_logits = kl.Dense(num_cat, name="dense_logits_{}".format(num_cat))

        self.cur_attention_mask = None

    def forward_model(self, emb):
        extended_attention_mask = self.cur_attention_mask[:, tf.newaxis, tf.newaxis, :]

        # Since attention_mask is 1.0 for positions we want to attend and 0.0 for
        # masked positions, this operation will create a tensor which is 0.0 for
        # positions we want to attend and -10000.0 for masked positions.
        # Since we are adding it to the raw scores before the softmax, this is
        # effectively the same as removing these entirely.

        extended_attention_mask = tf.cast(extended_attention_mask, tf.float32)
        extended_attention_mask = (1.0 - extended_attention_mask) * -10000.0

        head_mask = [None] * self.config.num_hidden_layers

        enc_out = self.albert_model.albert.encoder(emb, extended_attention_mask, head_mask,
                                                   None, None, False)[0]

        x = enc_out[:, 0]
        x = self.dense_latent(x)
        x = self.dense_logits(x)

        return x

    def forward_embed(self, inp):
        embedding_out = self.embeddings(input_ids=inp["input_ids"], token_type_ids=inp["token_type_ids"])
        self.cur_attention_mask = inp["attention_mask"]

        return embedding_out
