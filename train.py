import os
import sys
import warnings
import transformers

with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)
    import tensorflow as tf

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')
import keras

sys.stderr = stderr
import argparse
import pandas as pd
import importlib
import pickle
import json
from sklearn.metrics import f1_score, precision_score, recall_score, confusion_matrix
from sklearn.utils.class_weight import compute_sample_weight, compute_class_weight
from sklearn import manifold
from mpl_toolkits.mplot3d import Axes3D
from keras import backend as K
from model import *
from preprocessing import *
import numpy as np
from functools import partial
from sklearn.model_selection import StratifiedShuffleSplit, StratifiedKFold

from datasets import DATASET


def extend_array(X1, X2):
    # Extend array X2 to X1 shape on axis 0
    num_tile = X1.shape[0] // X2.shape[0]
    num_remainder = X1.shape[0] % X2.shape[0]

    X2_extend = X2
    if num_tile > 1:
        if X2.ndim == 1:
            reps = num_tile
        else:
            reps = tuple([num_tile] + [1] * len(X2.shape[1:]))
        X2_extend = np.tile(X2, reps)
    if num_remainder > 0:
        X2_extend = np.concatenate([X2_extend, X2[:num_remainder]], axis=0)

    return X2_extend


def adjust_size_unlabeled(X, X_unl=None, y=None, sample_weights=None):
    if not X_unl is None:
        if X.shape[0] > X_unl.shape[0]:
            X_unl = extend_array(X, X_unl)
        elif X.shape[0] < X_unl.shape[0]:
            if not y is None:
                assert X.shape[0] == y.shape[0]
                y = extend_array(X_unl, y)
            if not sample_weights is None:
                assert sample_weights.shape[0] == X.shape[0]
                sample_weights = extend_array(X_unl, sample_weights)

            X = extend_array(X_unl, X)

    return X, X_unl, y, sample_weights


def stack_batch_encodings(X_batch):
    batch = {k: np.stack(v, axis=0).astype(np.int32) for k, v in X_batch.items()}
    return batch


def data_generator(X, tokenizer, X_unl=None, y=None, batch_size=128,
                   validation=False, sample_weights=None, fun_read_unlabeled=None,
                   fun_read_x=None, only_inputs=False):
    X_batch, X_unl_batch, y_batch, w_batch = [], [], [], []

    def reset_batches():
        nonlocal X_unl_batch, X_batch, y_batch, w_batch

        X_batch = []
        X_unl_batch = []
        y_batch = []
        w_batch = []

    reset_batches()

    if y is None and not sample_weights is None:
        raise ValueError("y should not be none if sample weight is provided")

    len_X = len(X)

    smallest_size = len_X
    biggest_size = smallest_size
    x_smallest = True
    if not X_unl is None:
        len_X_unl = len(X_unl)

        if len_X > len_X_unl:
            smallest_size = len_X_unl
            x_smallest = False
        else:
            biggest_size = len_X_unl

    ind = np.arange(biggest_size)
    ind_smallest = np.arange(smallest_size)
    len_batch = 0
    while True:

        if not validation:
            np.random.shuffle(ind)
            np.random.shuffle(ind_smallest)
        for idx_id, id in enumerate(ind):

            idx_id_smallest = idx_id % smallest_size

            if x_smallest:
                id_x = ind_smallest[idx_id_smallest]
                id_unl = id
            else:
                id_x = id
                id_unl = ind_smallest[idx_id_smallest]

            X_batch.append(X[id_x])

            if not y is None:
                y_batch.append(y[id_x])

            if not X_unl is None:
                X_unl_batch.append(X_unl[id_unl])

            if not sample_weights is None:
                w_batch.append(sample_weights[id_x])

            len_batch += 1
            if len_batch >= batch_size or (validation and idx_id == len(ind) - 1):
                if fun_read_x:
                    batch = fun_read_x(X_batch)
                else:
                    batch = preprocess_text(X_batch, tokenizer, only_inputs=only_inputs)

                label_batch = None
                if not y is None:
                    label_batch = np.stack(y_batch, axis=0)

                weights_batch = None
                if not sample_weights is None:
                    weights_batch = np.stack(w_batch, axis=0).astype(np.float32)

                unl_batch = None
                if not X_unl is None:
                    if fun_read_unlabeled:
                        unl_batch = fun_read_unlabeled(X_unl_batch)
                    else:
                        unl_batch = preprocess_text(X_unl_batch, tokenizer, only_inputs=only_inputs)

                batch_dict = {"inputs": batch, "labels": label_batch, "weights": weights_batch,
                              "inputs_unlabeled": unl_batch}
                yield batch_dict

                reset_batches()
                len_batch = 0


def common_arg_parser():
    """
    Create an argparse.ArgumentParser for run_mujoco.py.
    """
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--save_path', help='Path to save trained model to',
                        default="lstm.{epoch:02d}-{val_loss:.2f}.hdf5", type=str)
    parser.add_argument('--load_model', help='Model to load to start training', default=None, type=str)
    parser.add_argument('--lr', help='Learning rate', default=1e-3, type=float)
    parser.add_argument('--batch_size', help='Batch size', default=256, type=int)
    parser.add_argument('--num_hidden', help='Number of hidden dimension for lstm', default=32, type=int)
    parser.add_argument('--num_epochs', help='Number of epochs', default=50, type=int)
    parser.add_argument('--num_cat', help='Number of categories', default=2, type=int)
    parser.add_argument('--start_epoch', help='Starting epoch', default=0, type=int)
    parser.add_argument('--patience', help='Number of epoch without improvement before reducing lr', default=3,
                        type=int)
    parser.add_argument('--use_generator', help='Use generator during training', action="store_true", default=False)
    parser.add_argument('--use_at', default=False, action='store_true')
    parser.add_argument('--use_vat', default=False, action='store_true')
    parser.add_argument('--save_best_only', default=False, action='store_true',
                        help="Save weights with lower loss only")
    parser.add_argument('--kfold_training', type=int, help="Use Kfold training")
    parser.add_argument('--transfer', type=str, choices=DATASET, help="transfer learning from dataset")
    parser.add_argument('--trainable_embedding', default=False, action='store_true')
    parser.add_argument('--save_embedding', default=None, help="Save embedding matrix", type=str)
    parser.add_argument('--use_embedding', default=None, help="Load embedding matrix", type=str)
    parser.add_argument('--dataset', required=True, choices=DATASET,
                        help="chosen dataset", type=str)
    return parser


def get_sample_weight(y_train):
    y_reduced = np.argmax(y_train, axis=-1)
    classes = np.unique(y_reduced)
    class_weight = compute_class_weight(class_weight="balanced",
                                        classes=classes, y=y_reduced)
    dict_weight = {}
    for idx, w in enumerate(class_weight):
        dict_weight[classes[idx]] = w

    sample_weight = compute_sample_weight(dict_weight, y_reduced)

    return sample_weight


def kfold_train(num_split, args, len_seq, vocabulary_size, use_vat,
                X_train, y_train, X_train_unl, X_test, y_test, sample_weight):
    sss = StratifiedKFold(n_splits=num_split, shuffle=True, random_state=42)

    run = KFold_LSTM_Model(num_split, len_seq, vocabulary_size, 300, args.num_hidden, args.num_cat,
                           embedding_matrix=embedding_matrix, trainable_embedding=args.trainable_embedding,
                           out_latent=False, use_at=args.use_at, use_vat=use_vat, lr=args.lr, use_weights=True)

    iter = 0
    base_name = args.save_path[:-5]
    for train_ind, val_ind in sss.split(X_train, np.argmax(y_train, axis=-1)):
        x_fold_train = X_train[train_ind]
        y_fold_train = y_train[train_ind]
        sw_fold_train = sample_weight[train_ind]

        x_fold_val = X_train[val_ind]
        y_fold_val = y_train[val_ind]

        args.save_path = base_name + "_fold_{}.hdf5".format(iter)

        train_model(args, run[iter], use_vat, x_fold_train, y_fold_train, X_train_unl,
                    x_fold_val, y_fold_val, sw_fold_train)

        iter += 1
        print("Done iteration {} in {} Kfold split".format(iter, num_split))

    print("Evaluate kfold on test set")

    loss, accuracy, loss_cat = run.evaluate([X_test, y_test, X_test], np.ones_like(y_test), batch_size=args.batch_size,
                                            verbose=1)
    print("loss {} - accuracy {} - loss_cat {}".format(loss, accuracy, loss_cat))


def generate_lr_callback(patience=3, factor=0.5):
    def Callback_LR(LossList, lr_var, min_delta=0.1, patience=patience):
        # No early stopping for 2*patience epochs
        if len(LossList) // patience < 2:
            return False
        # Mean loss for last patience epochs and second-last patience epochs
        mean_previous = np.mean(LossList[::-1][patience:2 * patience])  # second-last
        mean_recent = np.mean(LossList[::-1][:patience])  # last

        delta_abs = np.abs(mean_recent - mean_previous)  # abs change
        delta_abs = np.abs(delta_abs / mean_previous)  # relative change
        if delta_abs < min_delta:
            print("*CB_LR* Loss didn't change much from last %d epochs" % (patience))
            print("*CB_LR* Percent change in loss value:", delta_abs * 1e2)
            print("Reducing lr value to {}".format(tf.keras.backend.get_value(lr_var) * factor))
            tf.keras.backend.set_value(lr_var, tf.keras.backend.get_value(lr_var) * factor)
            return True
        else:
            return False

    return Callback_LR


def train_model(args, run, use_vat, X_train, y_train, X_train_unl, X_val, y_val, sample_weight):
    if args.load_model:
        run.load_weights(args.load_model, by_name=True)

    callback_lr = generate_lr_callback(patience=args.patience, factor=0.5)

    len_X_train = len(X_train)
    if isinstance(X_train, transformers.BatchEncoding):
        len_X_train = len(list(X_train.values())[0])

    len_X_val = len(X_val)
    if isinstance(X_val, transformers.BatchEncoding):
        len_X_val = len(list(X_val.values())[0])

    steps_per_epoch = len_X_train // args.batch_size + 1
    val_steps = len_X_val // args.batch_size
    if val_steps % args.batch_size > 0:
        val_steps += 1

    train_gen = data_generator(X_train, run.tokenizer, y=y_train, X_unl=(X_train_unl if use_vat else None),
                               sample_weights=sample_weight, batch_size=args.batch_size,
                               fun_read_unlabeled=fun_read_unlabeled, only_inputs=run.use_simple_inputs())
    val_gen = data_generator(X_val, run.tokenizer, y=y_val, X_unl=(X_val if use_vat else None),
                             batch_size=args.batch_size, validation=True, only_inputs=run.use_simple_inputs())

    run.train_generator(train_gen, steps_per_epoch=steps_per_epoch,
                        epochs=args.num_epochs,
                        validation_data=val_gen, validation_steps=val_steps,
                        initial_epoch=args.start_epoch,
                        save_weights=args.save_path, callback_lr=callback_lr)


if __name__ == "__main__":

    parser = common_arg_parser()

    args = parser.parse_args()

    dataset = importlib.import_module(args.dataset)
    X_train, X_val, label_train, label_val, X_train_unl, fun_read_unlabeled = dataset.read_dataset()

    use_vat = args.use_vat and not X_train_unl is None
    args_layers = {"init_embedding_matrix": args.load_model is None, "trainable_embedding": args.trainable_embedding,
                   "hidden_dim": args.num_hidden,
                   "embedding_dim": 300}

    run = DistilBERT_Model(args.num_cat, args_layers, lr=args.lr,
                           use_at=args.use_at, use_vat=use_vat, name="distilbert_base")

    # run = ALBERT_Model(args.num_cat, args_layers, lr=args.lr,
    #                        use_at=args.use_at, use_vat=use_vat, name="albert_base")

    print("dataset read")

    y_train, y_val, = preprocess_labels(label_train, label_val)

    if not fun_read_unlabeled is None:
        fun_read_unlabeled = partial(fun_read_unlabeled, tokenizer=run.tokenizer)

    print("preprocessing done")

    sample_weight = get_sample_weight(y_train)

    if not args.kfold_training:

        train_model(args, run, use_vat, X_train, y_train, X_train_unl, X_val, y_val, sample_weight)
    else:
        kfold_train(args.kfold_training, args, len_seq, vocabulary_size, use_vat,
                    X_train, y_train, X_train_unl, X_val, y_val, sample_weight)
